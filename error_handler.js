"use strict";
window.debugging = (window.location.href.indexOf("localhost") || window.location.href.indexOf("devapp"));
window.client = {
    /*
     * JavaScript Client Detection
     * Original viazenetti GmbH (Christian Ludwig)
     * Modified by Jonathan White 
     */
    ScreenSize: (function getScreenSize() {
        var screenSize = '';
        if (screen.width) {
            var width = (screen.width) ? screen.width : '';
            var height = (screen.height) ? screen.height : '';
            screenSize += '' + width + " x " + height;
        }
        return screenSize;
    })(),
    Mobile: (function getIsMobile() {
        var mobile = /Mobile|mini|Fennec|Android|iP(ad|od|hone)/.test(navigator.appVersion);
        return mobile;
    })(),
    Cookies: (function getCookieEnabled() {
        var cookieEnabled = (navigator.cookieEnabled) ? true : false;

        if (typeof navigator.cookieEnabled == 'undefined' && !cookieEnabled) {
            document.cookie = 'testcookie';
            cookieEnabled = (document.cookie.indexOf('testcookie') != -1) ? true : false;
        }
        return cookieEnabled;
    })(),
    Flash: (function getFlashVersion() {
        var flashVersion = 'no check';
        if (typeof swfobject != 'undefined') {
            var fv = swfobject.getFlashPlayerVersion();
            if (fv.major > 0) {
                flashVersion = fv.major + '.' + fv.minor + ' r' + fv.release;
            } else {
                flashVersion = " - ";
            }
        }
        return flashVersion;
    })(),
    Browser: (function getBrowserName() {
        var nAgt = navigator.userAgent;
        var browser = navigator.appName;
        var version = '' + parseFloat(navigator.appVersion);
        var majorVersion = parseInt(navigator.appVersion, 10);
        var nameOffset, verOffset, ix;
        // Opera
        if ((verOffset = nAgt.indexOf('Opera')) != -1) {
            browser = 'Opera';
            version = nAgt.substring(verOffset + 6);
            if ((verOffset = nAgt.indexOf('Version')) != -1) {
                version = nAgt.substring(verOffset + 8);
            }
        }
        // MSIE
        else if ((verOffset = nAgt.indexOf('MSIE')) != -1) {
            browser = 'Microsoft Internet Explorer';
            version = nAgt.substring(verOffset + 5);
        }
        // Chrome
        else if ((verOffset = nAgt.indexOf('Chrome')) != -1) {
            browser = 'Chrome';
            version = nAgt.substring(verOffset + 7);
        }
        // Safari
        else if ((verOffset = nAgt.indexOf('Safari')) != -1) {
            browser = 'Safari';
            version = nAgt.substring(verOffset + 7);
            if ((verOffset = nAgt.indexOf('Version')) != -1) {
                version = nAgt.substring(verOffset + 8);
            }
        }
        // Firefox
        else if ((verOffset = nAgt.indexOf('Firefox')) != -1) {
            browser = 'Firefox';
            version = nAgt.substring(verOffset + 8);
        }
        // MSIE 11+
        else if (nAgt.indexOf('Trident/') != -1) {
            browser = 'Microsoft Internet Explorer';
            version = nAgt.substring(nAgt.indexOf('rv:') + 3);
        }
        // Other browsers
        else if ((nameOffset = nAgt.lastIndexOf(' ') + 1) < (verOffset = nAgt.lastIndexOf('/'))) {
            browser = nAgt.substring(nameOffset, verOffset);
            version = nAgt.substring(verOffset + 1);
            if (browser.toLowerCase() == browser.toUpperCase()) {
                browser = navigator.appName;
            }
        }
        // trim the version string
        if ((ix = version.indexOf(';')) != -1) version = version.substring(0, ix);
        if ((ix = version.indexOf(' ')) != -1) version = version.substring(0, ix);
        if ((ix = version.indexOf(')')) != -1) version = version.substring(0, ix);
        majorVersion = parseInt('' + version, 10);
        if (isNaN(majorVersion)) {
            version = '' + parseFloat(navigator.appVersion);
            majorVersion = parseInt(navigator.appVersion, 10);
        }
        return { Name: browser, Ver: version };
    })(),
    OS: (function() {
        {
            var unknown = '-';
            var nAgt = navigator.userAgent;
            var nVer = navigator.appVersion;
            var os = unknown;
            var clientStrings = [
                { s: 'Windows 3.11', r: /Win16/ },
                { s: 'Windows 95', r: /(Windows 95|Win95|Windows_95)/ },
                { s: 'Windows ME', r: /(Win 9x 4.90|Windows ME)/ },
                { s: 'Windows 98', r: /(Windows 98|Win98)/ },
                { s: 'Windows CE', r: /Windows CE/ },
                { s: 'Windows 2000', r: /(Windows NT 5.0|Windows 2000)/ },
                { s: 'Windows XP', r: /(Windows NT 5.1|Windows XP)/ },
                { s: 'Windows Server 2003', r: /Windows NT 5.2/ },
                { s: 'Windows Vista', r: /Windows NT 6.0/ },
                { s: 'Windows 7', r: /(Windows 7|Windows NT 6.1)/ },
                { s: 'Windows 8.1', r: /(Windows 8.1|Windows NT 6.3)/ },
                { s: 'Windows 8', r: /(Windows 8|Windows NT 6.2)/ },
                { s: 'Windows NT 4.0', r: /(Windows NT 4.0|WinNT4.0|WinNT|Windows NT)/ },
                { s: 'Windows ME', r: /Windows ME/ },
                { s: 'Android', r: /Android/ },
                { s: 'Open BSD', r: /OpenBSD/ },
                { s: 'Sun OS', r: /SunOS/ },
                { s: 'Linux', r: /(Linux|X11)/ },
                { s: 'iOS', r: /(iPhone|iPad|iPod)/ },
                { s: 'Mac OS X', r: /Mac OS X/ },
                { s: 'Mac OS', r: /(MacPPC|MacIntel|Mac_PowerPC|Macintosh)/ },
                { s: 'QNX', r: /QNX/ },
                { s: 'UNIX', r: /UNIX/ },
                { s: 'BeOS', r: /BeOS/ },
                { s: 'OS/2', r: /OS\/2/ },
                { s: 'Search Bot', r: /(nuhk|Googlebot|Yammybot|Openbot|Slurp|MSNBot|Ask Jeeves\/Teoma|ia_archiver)/ }
            ];
            for (var id in clientStrings) {
                var cs = clientStrings[id];
                if (cs.r.test(nAgt)) {
                    os = cs.s;
                    break;
                }
            }
            var osVersion = unknown;
            if (/Windows/.test(os)) {
                osVersion = /Windows (.*)/.exec(os)[1];
                os = 'Windows';
            }
            switch (os) {
            case 'Mac OS X':
                osVersion = /Mac OS X (10[\.\_\d]+)/.exec(nAgt)[1];
                break;
            case 'Android':
                osVersion = /Android ([\.\_\d]+)/.exec(nAgt)[1];
                break;
            case 'iOS':
                osVersion = /OS (\d+)_(\d+)_?(\d+)?/.exec(nVer);
                osVersion = osVersion[1] + '.' + osVersion[2] + '.' + (osVersion[3] | 0);
                break;
            }
        }
        return { Name: os, Ver: osVersion };
    })()
};
window.errorHandler = {
    LogErrorToSupport: function(ctgry, msg, fileurl, line, col, error) {
        // Errors are logged in this format: Ctgry - Web Url: - User Id: - Message: - File: - Line Number: - Call Stack:
        // Note that col & error are new to the HTML 5 spec and may not be 
        // supported in every browser.
        error = (error ? error : { stack: "No Stack Trace Available" });
        var extra = !col ? '' : ' Column: ' + col;
        var userAgent = (function (client) {
            var agent = "{ ";
            for (var key in client) {
                if (typeof client[key] === 'object') {
                    agent += key + ":{ ";
                    for (var x in client[key]) {
                        agent += x + ": " + client[key][x] + " - ";
                    }
                    agent += "} - ";
                } else {
                    agent += key + ": " + client[key] + " - ";
                }
            }
            return agent + " }";
        })(window.client);
        var errorFormatted = {
            category: ctgry,
            message: msg,
            webUrl: window.location.href,
            fileUrl: fileurl,
            lineNumber: line + extra,
            callStack: error.stack,
            userId: window.userId,
            userAgent: userAgent
        };
        // Logging error to server:
        (function logErrorToServer(errorFormatted) {
            var xmlhttp;
            var parsedError = (function() {
                var parsedError = "";
                for (var key in errorFormatted) {
                    if (parsedError != "") {
                        parsedError += "&";
                    }
                    parsedError += key + "=" + encodeURIComponent(errorFormatted[key]);
                }
                return parsedError;
            })(errorFormatted);

            if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
                xmlhttp = new XMLHttpRequest();
            } else { // code for IE6, IE5
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
            }
            xmlhttp.open("POST", "/Log", true);
            xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            xmlhttp.send(parsedError);

            // If you return true, then error alerts (like in older versions of 
            // Internet Explorer) will be suppressed.
            var suppressErrorAlert = true;
            return suppressErrorAlert;
        })(errorFormatted);
    },
    LogErrorToGA: function(category, msg, fileurl, line, col, error) {
        //jS error
        if (!window.debugging && typeof _gaq != "undefined") {
            _gaq.push([
                '_trackEvent',
                category,
                window.location.href,
                'File Url: ' + fileurl + ' - Line Number: ' + line + (col ? ":" + col : "") + ' - Message: ' + msg + ' - Stack: ' + error.stack + " - User: " + window.userId,
                true
            ]);
        }
    }
};
(function addErrorEventListeners() {
    window.userId = (function() {
        var name = "regId=",
            ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') c = c.substring(1);
            if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
        }
        return "";
    })();
    // JS Compile Time Catch
    window.onerror = function(msg, fileurl, line, col, error) {
        window.errorHandler.LogErrorToSupport("JavaScript Error", msg, fileurl, line, col, error);
        window.errorHandler.LogErrorToGA("JavaScript Error", msg, fileurl, line, col, error);
    };
    // JS Run Time & Resource Catch
    window.addEventListener('error', function (e) {
        if (e.target !== window) {
            var message = (e.message || ""), fileUrl = e.filename || "", lineNumber = e.lineno || "", colNumber = e.colno || "", errorObj = e.error || "", errorType = "";
            message = message || "Resource Failed to load - Status 404";
            fileUrl = e.target.src || e.target.href;
            errorType = "404 Error";
            window.errorHandler.LogErrorToSupport(errorType, message, fileUrl, lineNumber, colNumber, errorObj);
            window.errorHandler.LogErrorToGA(errorType, message, fileUrl, lineNumber, colNumber, errorObj);
        }
    }, true);
})();